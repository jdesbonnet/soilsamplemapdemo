<%@page import="org.apache.commons.lang.mutable.MutableInt"%>
<%@page import="org.apache.commons.lang.mutable.MutableDouble"%>
<%@include file="_header.jsp" %><%

List<Sample> samples = em
.createQuery("select s from Sample s")
.getResultList();

HashMap<String,MutableDouble> summerHash = new HashMap<String,MutableDouble>();
HashMap<String,MutableDouble> summer2Hash = new HashMap<String,MutableDouble>();
HashMap<String,MutableInt> nHash = new HashMap<String,MutableInt>();

HashMap<String,MutableDouble> minHash = new HashMap<String,MutableDouble>();
HashMap<String,MutableDouble> maxHash = new HashMap<String,MutableDouble>();


for (Sample sample : samples) {
	String sampleTypeCode = sample.getSampleType();
	Double value = sample.getSampleValue();
	MutableDouble summer = summerHash.get(sampleTypeCode);
	if (summer == null) {
		summer = new MutableDouble(0);
		summerHash.put(sampleTypeCode,summer);
	}
	summer.add(value);
	
	MutableDouble summer2 = summer2Hash.get(sampleTypeCode);
	if (summer2 == null) {
		summer2 = new MutableDouble(0);
		summer2Hash.put(sampleTypeCode,summer2);
	}
	summer2.add(value*value);
	
	MutableDouble min = minHash.get(sampleTypeCode);
	if (min == null) {
		min = new MutableDouble(1e10);
		minHash.put(sampleTypeCode,min);
	}
	if (value < (Double)min.getValue()) {
		min.setValue(value);
	}
	
	MutableDouble max = maxHash.get(sampleTypeCode);
	if (max == null) {
		max = new MutableDouble(0);
		maxHash.put(sampleTypeCode,max);
	}
	if (value > (Double)max.getValue()) {
		max.setValue(value);
	}
	
	MutableInt n = nHash.get(sampleTypeCode);
	if (n == null) {
		n = new MutableInt(0);
		nHash.put(sampleTypeCode,n);
	}
	n.increment();
	
}

for (String sampleTypeCode : summerHash.keySet()) {
	List<SampleType> list = em.createQuery("from SampleType where code=:code")
	.setParameter("code", sampleTypeCode)
	.getResultList();
	
	SampleType sampleType;
	if (list.size()==0) {
		sampleType = new SampleType();
		sampleType.setCode(sampleTypeCode);
		em.persist(sampleType);
	} else {
		sampleType = list.get(0);
	}
	
	double sum = (Double)summerHash.get(sampleTypeCode).getValue();
	double sum2 = (Double)summer2Hash.get(sampleTypeCode).getValue();
	
	int n = (Integer)nHash.get(sampleTypeCode).getValue();
	double N = (double)n;
	
	double mean = sum / (double)N;
	//double std = Math.sqrt (N*sum - sum2) / N;
	double variance = (sum2 - (sum*sum)/N) / N;
	double std = Math.sqrt(variance);
	double min = (Double)minHash.get(sampleTypeCode).getValue();
	double max = (Double)maxHash.get(sampleTypeCode).getValue();
	
	sampleType.setMinimum(min);
	sampleType.setMaximum(max);
	sampleType.setVariance(variance);
	sampleType.setMean(mean);
	
	
	
	out.println ("Mean for " + sampleTypeCode + ":"
			+ " N=" + N
			+ " mean=" + mean 
			+ " std=" + std
			+ " min=" + min
			+ " max=" + max
			+ "<br>\n");
}
%>