
<style>
#mapContainer {
	width:100%;
	height:450px;
}
.gradientScaleTable {
	width:100%;
}
.gradientScaleTable td {
	padding:3px;
	padding-left:8px;
}
.gradientScaleTable span {
	font-size: 7pt;
	padding:1px;
	background-color:white;
	color:black;
	opacity: 0.8;
	border-radius:2px;
}
</style>

##<h1 class="page-header">$project.name</h1>

<div class="row" style="margin-bottom:0.5em;">
<div class="col-md-12">
<form class="form-inline">
&nbsp; &nbsp;
<label>Data layer</label>
<select class="form-control" id="layer_select">
#foreach ($sampleType in $sampleTypes)
<option value="${sampleType.code}"> 
#if ($sampleType.name)
$!{sampleType.name}
#else
${sampleType.code}
#end 
##($!{sampleType.unit})
</option>
#end
</select>

&nbsp;
<button type="button" class="btn btn-default"  id="play"><span class="glyphicon glyphicon-play" 
title="auto cycle through layers" ></span></button>	
<button type="button" class="btn btn-default" id="pause" disabled><span class="glyphicon glyphicon-pause" 
title="stop auto cycle through layers"></span></button>	


&nbsp;
<input type="checkbox" id="cb_heatmap" checked/> Heatmap
&nbsp;
<input type="checkbox" id="cb_markers" /> Show samples

</form>
</div>
</div>

<div id="row">
<div class="col-md-9">
<div id="mapContainer"></div>
<div id="gradientScale"></div>
<table style="width:100%">
<tr>
<td width="25%">Layer: <span id="layer"></span></td>
<td width="15%">Unit: <span id="unit"></span></td>
<td width="15%">Min: <span id="min"></span></td>
<td width="15%">Max: <span id="max"></span></td>
<td width="15%">Mean: <span id="mean"></span></td>
<td width="15%">σ: <span id="std"></span></td>
</tr>
</table>
</div>
<div class="col-md-3">

</div>
</div>



<script src="https://maps.googleapis.com/maps/api/js?libraries=geometry,visualization"></script>

<script>

var sampleTypes = [
#foreach ($sampleType in $sampleTypes)
{ code: "${sampleType.code}", name: "${sampleType.name}"	, unit:"${sampleType.unit}" },
#end
];

// Make hash so that SampleType can be retrieved by its code
var sampleTypeHash = {};
sampleTypes.forEach(function(sampleType){
	sampleTypeHash[sampleType.code]=sampleType;
});

// Some info on color gradient scales:
// http://www.andrewnoske.com/wiki/Code_-_heatmaps_and_color_gradients
var customGradient = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
];

// Function to round to x decimal places
function precise_round(num,decimals){
    var sign = num >= 0 ? 1 : -1;
    return (Math.round((num*Math.pow(10,decimals))+(sign*0.001))/Math.pow(10,decimals)).toFixed(decimals);
}


// All the soil samples data here
var dataset = [];

// Array for markers. To display set map property to map, to hide,
// set map property to null.
var mapMarkers = [];
var mapMarkerHash = {};

// Data pertaining to current heatmap here. Use MVCArray so that updates made
// to the array will automatically update the map.
var heatMapData = new google.maps.MVCArray();

// If set true UI will auto cycle through the layers
var autoLayerAdvance = false;
var currentLayerIndex = 0;
var autoLayerAdvanceInterval = 3000;



// Icon used to represent a sampling point
var sampleMarker = {
    url: 'circle-red.png',
    size: new google.maps.Size(16, 16),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(8,8)
};

// Info window which opens on marker click
var infowindow = new google.maps.InfoWindow({
	content: "..."
});
	
  
  

// Heatmap hack so that it does not change with zoom level
// http://stackoverflow.com/questions/12291459/google-maps-heatmap-layer-point-radius
// Working example here: http://jsbin.com/rorecuce/1/

var map, pointarray, heatmap;
var TILE_SIZE = 256;

      //Mercator --BEGIN--
      function bound(value, opt_min, opt_max) {
          if (opt_min !== null) value = Math.max(value, opt_min);
          if (opt_max !== null) value = Math.min(value, opt_max);
          return value;
      }

      function degreesToRadians(deg) {
          return deg * (Math.PI / 180);
      }

      function radiansToDegrees(rad) {
          return rad / (Math.PI / 180);
      }

      function MercatorProjection() {
          this.pixelOrigin_ = new google.maps.Point(TILE_SIZE / 2,
          TILE_SIZE / 2);
          this.pixelsPerLonDegree_ = TILE_SIZE / 360;
          this.pixelsPerLonRadian_ = TILE_SIZE / (2 * Math.PI);
      }


      MercatorProjection.prototype.fromLatLngToPoint = function (latLng,
      opt_point) {
          var me = this;
          var point = opt_point || new google.maps.Point(0, 0);
          var origin = me.pixelOrigin_;

          point.x = origin.x + latLng.lng() * me.pixelsPerLonDegree_;

          // NOTE(appleton): Truncating to 0.9999 effectively limits latitude to
          // 89.189.  This is about a third of a tile past the edge of the world
          // tile.
          var siny = bound(Math.sin(degreesToRadians(latLng.lat())), - 0.9999,
          0.9999);
          point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -me.pixelsPerLonRadian_;
          return point;
      };

      MercatorProjection.prototype.fromPointToLatLng = function (point) {
          var me = this;
          var origin = me.pixelOrigin_;
          var lng = (point.x - origin.x) / me.pixelsPerLonDegree_;
          var latRadians = (point.y - origin.y) / -me.pixelsPerLonRadian_;
          var lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);
          return new google.maps.LatLng(lat, lng);
      };

      //Mercator --END--
      
	  var desiredRadiusPerPointInMeters = 13;
	  
      function getNewRadius() {
          
		  
          var numTiles = 1 << map.getZoom();
          var center = map.getCenter();
          var moved = google.maps.geometry.spherical.computeOffset(center, 10000, 90); /*1000 meters to the right*/
          var projection = new MercatorProjection();
          var initCoord = projection.fromLatLngToPoint(center);
          var endCoord = projection.fromLatLngToPoint(moved);
          var initPoint = new google.maps.Point(
            initCoord.x * numTiles,
            initCoord.y * numTiles);
           var endPoint = new google.maps.Point(
            endCoord.x * numTiles,
            endCoord.y * numTiles);
        var pixelsPerMeter = (Math.abs(initPoint.x-endPoint.x))/10000.0;
        var totalPixelSize = Math.floor(desiredRadiusPerPointInMeters*pixelsPerMeter);
        //console.log(totalPixelSize);
        return totalPixelSize;
         
      }





function initializeMap() {

	var centerPoint = new google.maps.LatLng(53.283,-9.07);
	var mapOptions = {
		center: centerPoint,
		zoom: 16
	};

	map = new google.maps.Map(document.getElementById('mapContainer'),
		mapOptions);
				 
	// Retrieve data from server: called only once.        
    $.ajax({
		url: "project-data.async.jsp",
		data: "project_id=" + ${project.id},
	}).done(function (data) {
		
		// Store data retrieved from server into 'dataset' global array
		data.forEach (function(sample) {
			dataset.push(sample);
		});
		
		jQuery("#layer_select").trigger("change");
		
		// Create marker objects
		dataset.forEach(function(sample) {
			var samplePoint = new google.maps.LatLng(
				sample.latitude + 0.0002,
				sample.longitude - 0.0005);
			
			var marker = new google.maps.Marker({
				position: samplePoint,
				//map: map,
				//icon: 'circle-red.png',
				icon: sampleMarker,
				title: sample.sid,
				sample: sample
			});
			
			// Listen to marker click event to open infowindow with info about sample
			google.maps.event.addListener(marker, 'click', function() {
				var html = "<b>" + this.sample.sid + "</b>";
				html += " lat: " + this.sample.latitude.toFixed(5);
				html += " lon: " + this.sample.longitude.toFixed(5);
				html += "<table border='1'><tr>";
				for (var i = 0; i < sampleTypes.length; i++) {
					html += "<td>" + sampleTypes[i].code + " &nbsp; ";
					html += sample[sampleTypes[i].code] + " " + sampleTypes[i].unit;
					html += "</td>";
					if (i%3==2) html += "</tr><tr>";
				}
				html += "</table>";
				infowindow.setContent(html);
    			infowindow.open(map,this);
			});
    	
    		// Store makers in mapMarkers global array (for display and hide)
			mapMarkers.push(marker);
			
			// Also in hash for retrival by SampleID
			mapMarkerHash[sample.sid] = marker;		
		});
		
		// Create Google Maps HeatMap. Like to heatMapData MVCArray. Any changes
		// to that array will automatically update the heathap.
		heatmap = new google.maps.visualization.HeatmapLayer({
  			data: heatMapData,
  			radius: getNewRadius(),
  			gradient: customGradient
		});
		heatmap.setMap(map);
		
		// Needed for the non dissipation hack		  
		google.maps.event.addListener(map, 'zoom_changed', function () {
              heatmap.setOptions({radius:getNewRadius()});
		});
		
	}).fail(function(xhr,errorMessage,error){
		alert ('some error occured ' + errorMessage);
		console.log(error);
	});
	
    
}

// Init code goes here: runs when DOM is ready.
jQuery(function(){

	initializeMap();
	
	jQuery("#cb_heatmap").change(function(){
		heatmap.setMap($(this).is(":checked") ? map : null);
	});
	jQuery("#cb_markers").change(function(){
		var ischecked = $(this).is(":checked");
		mapMarkers.forEach(function(marker){
			marker.setMap(ischecked ? map : null);
		});
	});
	jQuery("#play").click(function(){
		autoLayerAdvance = true;
			document.getElementById("pause").disabled = false;
		document.getElementById("play").disabled = true;
	});
	jQuery("#pause").click(function(){
		autoLayerAdvance = false;
		document.getElementById("play").disabled = false;
		document.getElementById("pause").disabled = true;
	});
	// If layer select has been changed, update heatmap data to new layer
	jQuery("#layer_select").change(function(){
	
		var layer = $(this).val();
		
		heatMapData.clear();
		var maxValue = 0;
		var minValue = 1e6;
		var sum = 0;
		var sum2 = 0;
		dataset.forEach(function(sample){
			var value = sample[layer];
			var samplePoint = new google.maps.LatLng(
				sample.latitude + 0.0002,
				sample.longitude - 0.0005);
			heatMapData.push ({
				location: samplePoint,
				weight: value
			});
			if (value>maxValue) {
				maxValue = value;
			}
			if (value<minValue) {
				minValue = value;
			}
			sum += value;
			sum2 += value*value;
			
			//console.log(mapMarkerHash[sample.sid]);
			if (mapMarkerHash[sample.sid]) {
				mapMarkerHash[sample.sid].setTitle (
					sample.sid 
					+ " " + layer  
					+ "=" + value);
			}
		});
		
		var d = maxValue / customGradient.length;
		var v = 0;
		cellWidth = 100 / customGradient.length;
		var html = "";
		html+= "<table class='gradientScaleTable'><tr>";
		customGradient.forEach(function(color){
			html += "<td style='width:" + cellWidth + "%;background-color:" + color + "'><span>" 
				+ precise_round(v,2) + "</span></td>";
			v += d;
		});
		html += "</tr></table>";
		jQuery("#gradientScale").html(html);
		
		jQuery("#layer").html(layer);
		jQuery("#min").html(minValue);
		jQuery("#max").html(maxValue);
		var N = dataset.length;
		jQuery("#mean").html(precise_round(sum/N,3));
		var variance = (sum2 - (sum*sum)/N) / N;
		var std = Math.sqrt(variance);
		jQuery("#std").html(precise_round(std,3));
		
		// TODO Use hash to avoid exhaustive search of sampleTypes
		sampleTypes.forEach(function(sampleType){
			if (layer === sampleType.code) {
				jQuery("#unit").html(sampleType.unit);
				if (sampleType.name != null) {
					jQuery("#layer").html(sampleType.name);
				}
			}
		});
		//alert ('maxValue=' + maxValue);
	});
	
	
	// Function called periodically to facilitate auto layer advance
	setInterval (function() {

		if (autoLayerAdvance===true) {
			var currentLayer = jQuery("#layer_select").val();
			var currentLayerIndex = -1;
			var opts = $("#layer_select OPTION");
			for (var i = 0; i < opts.length; i++) {
				if (jQuery(opts.get(i)).val() == currentLayer) {
					currentLayerIndex = i;
				}
			}
			if (currentLayerIndex === -1) {
				console.log ("layer not found");
				return;
			}
			currentLayerIndex++;
			if (currentLayerIndex >= opts.length-1) {
				currentLayerIndex = 0;
			}
			$("#layer_select").val(sampleTypes[currentLayerIndex].code);
			$("#layer_select").trigger("change");
			
		}
	},autoLayerAdvanceInterval);
});
</script>
