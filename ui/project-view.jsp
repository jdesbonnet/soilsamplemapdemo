<%@include file="_header.jsp" %><%

Project project = em.find(Project.class,new Long(request.getParameter("project_id")));

context.put ("project",project);

List<SampleType> sampleTypes = em.createQuery("from SampleType order by code")
.getResultList();

context.put ("sampleTypes", sampleTypes);

context.put ("pageId","project_view");
context.put ("pageMenu","projects");

templates.merge ("/master.vm",context,out);
%>