<%@include file="_header.jsp" %><%
Project project = new Project();
project.setName(request.getParameter("name"));
project.setLatitude(new Double(request.getParameter("latitude")));
project.setLongitude(new Double(request.getParameter("longitude")));
em.persist(project);
response.sendRedirect("project-view.jsp?c_id=" + project.getId());
%>