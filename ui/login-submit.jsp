<%@include file="_header.jsp"%><%String username = request.getParameter("username");
	String password = request.getParameter("password");
	
	List<User> users;
	users = em.createQuery("SELECT u from User u where u.email = :username")
		.setParameter("username", username)
		.getResultList();
	

	if (users.size() == 0) {
		session.setAttribute("alert", "User not found.");
		response.sendRedirect ("login.jsp?message=User+not+found");
		return;
		//throw new ServletException("user not found");
	}
	
	if (users.size() > 1 ) {
		throw new ServletException ("More than one user with username '" + username + "' found.");
	}

	user = users.get(0);
	
	if (user.getLifecycleState() != LifecycleState.NORMAL) {
		throw new ServletException ("User '" + username + "' disabled.");
	}
	
	//password = AuthenticationToken.hash(password);
	
	if (user.getPassword() == null || password.equals(user.getPassword())) {
		
		// Password match (or null password in user db). Create session.
		
		session.setAttribute ("user_id", user.getId());
		session.setAttribute ("username", user.getEmail());
		
		
		// Update lastLogin in User table (TODO: use a different mechanism.. want to keep User table read-mostly)
		user.setLastLogin(new Date());
		
		// If a 'next' parameter given with login jump to that 
		String next = request.getParameter("next");
		if (next != null && !next.endsWith("logout.jsp")) {
			response.sendRedirect(next);
			return;
		} 
		
		//ActivityLog.logEvent(em, user, ActivityType.LOGIN, null, request);

		// If 'next' param in POST redirect there
		if (request.getParameter("next")!=null) {
			response.sendRedirect (request.getParameter("next"));
			return;
		}
		
		response.sendRedirect ("project-list.jsp");

	}
	
	
	response.sendRedirect ("login.jsp?alert=Wrong%20email%20or%20password.%20Try%20again.");%>

%>