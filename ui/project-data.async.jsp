<%@include file="_header.jsp" %><%

Project project = em.find(Project.class,new Long(request.getParameter("project_id")));

List<Sample> samples = em
.createQuery("select s from Sample s where s.project.id=:project_id order by s.projectSampleId")
.setParameter("project_id", project.getId())
.getResultList();

HashMap<String,HashMap<String,Sample>> sampleHash = new HashMap<String,HashMap<String,Sample>>();
HashMap<String,Double> sampleIdToLatitudeHash = new HashMap<String,Double>();
HashMap<String,Double> sampleIdToLongitudeHash = new HashMap<String,Double>();

for (Sample sample : samples) {
	HashMap<String,Sample> h = sampleHash.get(sample.getProjectSampleId());
	if (h == null) {
		h = new HashMap<String,Sample>();
		sampleHash.put(sample.getProjectSampleId(),h);
	}
	h.put(sample.getSampleType(), sample);
	sampleIdToLatitudeHash.put(sample.getProjectSampleId(),sample.getLatitude());
	sampleIdToLongitudeHash.put(sample.getProjectSampleId(),sample.getLongitude());

}

response.setContentType ("application/json");
out.write ("[\n");

boolean first = true;
for (String sampleId : sampleHash.keySet()) {
	if (first) {
		first = false;
	} else {
		out.write (",");
	}
	out.write ("{");
	out.write ("\"sid\":\"" + sampleId + "\"");
	out.write (",\"latitude\":" + sampleIdToLatitudeHash.get(sampleId));
	out.write (",\"longitude\":" + sampleIdToLongitudeHash.get(sampleId));
	for (String sampleType : sampleHash.get(sampleId).keySet()) {
		Sample sample = sampleHash.get(sampleId).get(sampleType);
		out.write (", \"" + sampleType + "\":" + sample.getSampleValue());
	}
	out.write ("}\n");
}
out.write ("]\n");

%>