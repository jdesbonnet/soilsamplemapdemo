<%
TemplateRegistry templates= TemplateRegistry.getInstance();

/*
 * Initialize our own templating wrapper
 */
if (! templates.isInitialized()) {
	
	try {
		templates.init(
		getServletContext().getRealPath("/ui/templates"));
	} catch (ie.wombat.framework.AppException e) {
		throw new ServletException(e.toString());
	}
}
Context context = new Context(request,response);

context.put("GOOGLE_MAPS_API_KEY","AIzaSyAawWTQfv4Vc4uOOK5jhAEA6uvQX47iKnM");
//context.put("ASSETS","./include");
context.put("INCLUDE","./include");
context.put("VERSION",SSMDemo.VERSION);
context.put("formatUtil", new FormatUtil());
context.put("jsonUtil", new JSONUtil());
//context.put("user",user);
context.put("contextPath", getServletContext().getContextPath());
context.put("jsp",this); // enables static methods in JSP script to be available in template

//context.put("STRIX_SYSTEM","http://strix.wombat.ie/STRIX");
context.put("STRIX_SYSTEM","http://strixcm.com/STRIX");

// CDN
context.put("JQUERY_SCRIPT","//code.jquery.com/jquery-1.11.0.min.js");
context.put("BOOTSTRAP_ROOT","https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0");
context.put("FONT_AWESOME_ROOT","//maxcdn.bootstrapcdn.com/font-awesome/4.2.0");
// Local
//context.put("JQUERY_SCRIPT","./include/jquery-1.11.1.min.js");
//context.put("BOOTSTRAP_ROOT","./bootstrap-3.2.0-dist");
//context.put("FONT_AWESOME_ROOT","./font-awesome-4.2.0"); 
%>