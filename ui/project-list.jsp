<%@include file="_header.jsp" %><%

List<Project> projects = em
	.createQuery("SELECT p FROM Project p order by p.id")
	.getResultList();

context.put ("projects",projects);
context.put ("pageId","project_list");
context.put ("pageTitle","Project List");
context.put ("pageMenu","projects");
templates.merge ("/master.vm",context,out);
%>