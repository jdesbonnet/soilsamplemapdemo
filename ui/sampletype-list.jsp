<%@include file="_header.jsp" %><%

List<SampleType> sampleTypes = em
	.createQuery("SELECT st FROM SampleType st order by st.code")
	.getResultList();

context.put ("sampleTypes",sampleTypes);
context.put ("pageId","sampletype_list");
context.put ("pageTitle","Sample Type List");
context.put ("pageMenu","sampletypes");
templates.merge ("/master.vm",context,out);
%>