<%@page 
contentType="text/html; charset=UTF-8"
errorPage="error.jsp"
%><%@page 
import="java.io.File"
import="java.io.BufferedReader"
import="java.io.FileReader"
import="java.nio.ByteBuffer"
import="java.util.Base64"
import="java.util.List"
import="java.util.ArrayList"
import="java.util.Date"
import="java.util.Map"
import="java.util.HashMap"
import="java.util.EnumSet"
import="ie.wombat.template.TemplateRegistry"
import="ie.wombat.template.Context"
import="ie.wombat.ssmdemo.*"
import="javax.persistence.EntityManager"
import="net.sf.json.util.JSONUtils"
%><%

//All parameters to be interpreted as UTF8 encoded
request.setCharacterEncoding("UTF-8");

/* errorPage="/jsp/error.jsp" */

EntityManager em = HibernateUtil.getEntityManager();
em.getTransaction().begin();

// TODO: auth check

// TODO: this check should be at the highest level. But as trainee modules and admin modules
// are at the same level this is problematic right now.

Long userId = (Long)session.getAttribute("user_id");
User user = null;



if (userId == null) {
	

	String servletPath = request.getServletPath();
	System.err.println ("servletPath="+ servletPath);
	if ( servletPath.endsWith("login.jsp") 
	|| servletPath.endsWith("login-submit.jsp")
	|| servletPath.endsWith("forgot-password.jsp")
	|| servletPath.endsWith("forgot-password-submit.jsp")
	|| servletPath.endsWith("error.jsp")
	|| servletPath.endsWith("homepage.jsp")
	|| servletPath.endsWith("logout.jsp") )
	{
		System.err.println ("Script does not require login: " + servletPath);
		// The above scripts must be able to run without a valid session. 
		// Allow execution to continue.
	} else {
		
		// A request which requires a valid login has occured. For AJAX request
		// return an error message. For regular requests redirect to login page.
		
		if (request.getAttribute("ajaxRequest") != null || servletPath.endsWith(".async.jsp") ) {
			// TODO: consider creating new Exception class for AJAX errors
			// Nothing can be done about AJAX request outside of session except to report error
			// Flag this error as AJAX error: display short message.
			
			request.setAttribute("ajaxRequest","true");
			throw new ServletException ("Session has expired. A new login is required.");
		} else {
			// Most common case: redirect to login screen
			String requestUri = request.getRequestURI();
			if (request.getQueryString() !=null) {
				requestUri = requestUri + "?" + request.getQueryString();
			}
			System.err.println ("auth fail: redirecting to main login page");
			
			response.sendRedirect (request.getContextPath() + "/ui/login.jsp"
					+ "?next=" + java.net.URLEncoder.encode(requestUri)
							);
			
			return;
		}
	}
} else {
	
	user = em.find(User.class,userId);
	//context.put ("user",user);
}

%><%!

%><%{
	String[] path = request.getServletPath().split("/");
	String scriptName = path[path.length-1];
					
	// TODO: need to make more efficient
	boolean opAllowed = true;
	
	
	
}%><%@include file="_header_template_init.jsp" %><% 
if (session.getAttribute("alert")!=null) {
	context.put ("alert", session.getAttribute("alert"));
	session.removeAttribute("alert");
}
%>
