package ie.wombat.ssmdemo;

import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.lucene.util.Version;


public class SSMDemo   implements ServletContextListener {

	private static final Logger log = Logger.getLogger(SSMDemo.class);
	
	public static final Version LUCENE_VERSION = Version.LUCENE_46;
	//public static final SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat timestampFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
	public static final String VERSION = "0.1.0";
	
	// For testing, get API to delay to simulate load/slow network
	public static final long apiResponseDelay = 0;
	
	private static Properties configuration = new Properties();
	private static File confFile;
	private static long confLoadedTime=0L;
	
	public static void loadConfigurationProperties () {
		System.err.println ("Loading configuration...");

		Properties properties = new Properties() ;
		try {
			properties.load(new FileReader(confFile));
		} catch (Exception e1) {
			log.error ("Unable to load config.properties at " + confFile.getPath());
		}
		configuration = properties;
		confLoadedTime = System.currentTimeMillis();
	}
	
	public static String getConfiguration (String key) {
		
		// If configuration file has been edited, reload
		if (confFile.lastModified() > confLoadedTime) {
			loadConfigurationProperties();
		}
		// TODO: should values be trim() before returning?
		return (String)configuration.get(key);
	}
	
	
	public static String getConfiguration (String key, String defaultValue) {
		String value = getConfiguration(key);
		return value == null ? defaultValue : value;
	}
	
	/**
	 * Return the value of a boolean configuration property. Returns true if 
	 * set to "true", false for all other values or if not present.
	 * @param key
	 * @return
	 */
	public static boolean getConfigurationBoolean (String key) {
		return "true".equals(getConfiguration(key));
	}
	
	/**
	 * Return the value of a boolean configuration property and specify a 
	 * default value if the configuration is missing. Returns true if 
	 * set to "true", false if missing or for all other values.
	 * @param key
	 * @param defaultValue 
	 * @return
	 */
	public static boolean getConfigurationBoolean (String key, boolean defaultValue) {
		if (getConfiguration(key) == null) {
			return defaultValue;
		}
		return getConfigurationBoolean(key);
	}
	
	/**
	 * Return the value of a integer configuration property. 
	 * @param key
	 * @return
	 */
	public static int getConfigurationInt (String key) {
		return Integer.parseInt(getConfiguration(key));
	}

	/**
	 * Return the value of a integer configuration property and specify a 
	 * default value if the configuration is missing. 
	 * @param key
	 * @param defaultValue 
	 * @return
	 */
	public static int getConfigurationInt (String key, int defaultValue) {
		if (getConfiguration(key) == null) {
			return defaultValue;
		}
		return getConfigurationInt(key);
	}
	
	//Logger log = Logger.getLogger(AppStart.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
	
		System.err.println ("*** STARTING SSMDemo");
		
		BasicConfigurator.configure();
		
		File appRoot = new File(event.getServletContext().getRealPath("/"));
		
		confFile = new File(appRoot,"WEB-INF/config.properties");
	
	}
	
}
