package ie.wombat.ssmdemo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatUtil {

	private static SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
	private static SimpleDateFormat timestampDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
	private static SimpleDateFormat timestampMinuteDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");

	public static String numberFormat (Double d) {
		return String.format("%.2f", d);
	}
	
	public static String dateFormat (Date d) {
		if (d == null) {
			return "(null)";
		}
		return df.format(d);
	}
	
	public static Date dateParse (String d) {
		try {
			return df.parse(d);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String timestampFormat (Date d) {
		if (d == null) {
			return "(null)";
		}
		return timestampDateFormat.format(d);
	}
	public static Date timestampParse (String d) {
		try {
			return timestampDateFormat.parse(d);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String timestampMinuteFormat (Date d) {
		if (d == null) {
			return "(null)";
		}
		return timestampMinuteDateFormat.format(d);
	}
	
	/**
	 * Make safe for inclusion between double quotes of INPUT value attribute.
	 *
	 * @param s
	 * @return
	 */
	public static String inputFieldValue (String s) {
		if (s == null) {
			return "";
		}
		// TODO:
		return s;
	}
}
