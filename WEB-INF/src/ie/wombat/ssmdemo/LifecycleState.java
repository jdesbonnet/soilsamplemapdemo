package ie.wombat.ssmdemo;

public enum LifecycleState {
	NORMAL (0),
	DELETED (1);
	

	private int value ;

	LifecycleState ( int value ) {
		this.value = value ;
	}
}
