package ie.wombat.ssmdemo;

import ie.wombat.gis.OSIGridReference;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class CSVLoad {

	private static Logger log = Logger.getLogger(CSVLoad.class);
	
	public static List<Sample> loadCsv(File csvFile) throws IOException {
		BufferedReader r = new BufferedReader(new FileReader(csvFile));
		
		String description = r.readLine();
		log.info ("Loading " + csvFile.getPath() + " " + description);
		
		String[] fields = r.readLine().split(",");
		
		String line;
		int i;
		List<Sample> samples = new ArrayList<Sample>();
		
		while (  (line=r.readLine()) != null ) {
			String[] data = line.split(",");

			Integer easting = new Integer(data[1]);
			Integer northing = new Integer(data[2]);
			
			OSIGridReference gr = new OSIGridReference(easting, northing);
			
			double latitude = gr.getLatitudeDeg();
			double longitude = gr.getLongitudeDeg();
						
			for (i = 3; i < data.length; i++) {
				Sample sample = new Sample();
				sample.setLatitude(latitude);
				sample.setLongitude(longitude);
				sample.setProjectSampleId(data[0]);
				sample.setSampleType(fields[i]);
				sample.setSampleValue(new Double(data[i]));
				samples.add(sample);
			}
			
			
		}
		return samples;
		
	}
}
