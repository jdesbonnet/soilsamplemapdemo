package ie.wombat.ssmdemo;

import net.sf.json.util.JSONUtils;

public class JSONUtil {

	public static String quote(String s) {
		return JSONUtils.quote(s);
	}
}
