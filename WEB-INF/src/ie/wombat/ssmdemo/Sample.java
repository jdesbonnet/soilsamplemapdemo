package ie.wombat.ssmdemo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Sample {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	/** Sample code unique to project **/
	private String projectSampleId;
	
	private String sampleType;
	
	private Double sampleValue;
	
	private Date sampleDate = new Date();
	
	private Double latitude;
	private Double longitude;
	
	@ManyToOne
	private Project project;
	
	@ManyToOne
	private Instrument instrument;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getProjectSampleId() {
		return projectSampleId;
	}

	public void setProjectSampleId(String projectSampleId) {
		this.projectSampleId = projectSampleId;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public Double getSampleValue() {
		return sampleValue;
	}

	public void setSampleValue(Double sampleValue) {
		this.sampleValue = sampleValue;
	}

	public Date getSampleDate() {
		return sampleDate;
	}

	public void setSampleDate(Date sampleDate) {
		this.sampleDate = sampleDate;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
}
